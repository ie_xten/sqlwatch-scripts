/*
https://docs.sqlwatch.io/v/2.2/central-repository/configuration

*/

IF @@SERVERNAME = 'PRLAMWTSSQL01'
BEGIN

    EXEC [dbo].[usp_sqlwatch_user_repository_add_remote_instance] @sql_instance = 'PRLAMWTSSQL01', @hostname = 'PRLAMWTSSQL01', @sql_port = NULL, @sqlwatch_database_name = 'SQLWatch', @environment = 'Distributor', @linked_server_name = 'PRLAMWTSSQL01', @rmtuser = NULL, @rmtpassword = NULL

    EXEC [dbo].[usp_sqlwatch_user_repository_add_remote_instance] @sql_instance = 'PRLAMWTSSQL01B', @hostname = 'PRLAMWTSSQL01B', @sql_port = NULL, @sqlwatch_database_name = 'SQLWatch', @environment = 'Cluster', @linked_server_name = 'PRLAMWTSSQL01B', @rmtuser = NULL, @rmtpassword = NULL

    EXEC [dbo].[usp_sqlwatch_user_repository_add_remote_instance] @sql_instance = 'PRLAMWTSSSR03', @hostname = 'PRLAMWTSSSR03', @sql_port = NULL, @sqlwatch_database_name = 'SQLWatch', @environment = 'Booking & Search', @linked_server_name = 'PRLAMWTSSSR03', @rmtuser = NULL, @rmtpassword = NULL

    EXEC [dbo].[usp_sqlwatch_user_repository_add_remote_instance] @sql_instance = 'PRLAMWTSSSR04', @hostname = 'PRLAMWTSSSR04', @sql_port = NULL, @sqlwatch_database_name = 'SQLWatch', @environment = 'Booking & Search', @linked_server_name = 'PRLAMWTSSSR04', @rmtuser = NULL, @rmtpassword = NULL

    EXEC [dbo].[usp_sqlwatch_user_repository_add_remote_instance] @sql_instance = 'PRLAMWTSSSR05', @hostname = 'PRLAMWTSSSR05', @sql_port = NULL, @sqlwatch_database_name = 'SQLWatch', @environment = 'Booking & Search', @linked_server_name = 'PRLAMWTSSSR05', @rmtuser = NULL, @rmtpassword = NULL

    EXEC [dbo].[usp_sqlwatch_user_repository_add_remote_instance] @sql_instance = 'PRLCXWTSSQL01C', @hostname = 'PRLCXWTSSQL01C', @sql_port = NULL, @sqlwatch_database_name = 'SQLWatch', @environment = 'Cluster', @linked_server_name = 'PRLCXWTSSQL01C', @rmtuser = NULL, @rmtpassword = NULL

    EXEC [dbo].[usp_sqlwatch_user_repository_add_remote_instance] @sql_instance = 'PRLCXWTSSSR02', @hostname = 'PRLCXWTSSSR02', @sql_port = NULL, @sqlwatch_database_name = 'SQLWatch', @environment = 'Booking & Search', @linked_server_name = 'PRLCXWTSSSR02', @rmtuser = NULL, @rmtpassword = NULL

    EXEC [dbo].[usp_sqlwatch_user_repository_add_remote_instance] @sql_instance = 'PRLCXWTSSSR06', @hostname = 'PRLCXWTSSSR06', @sql_port = NULL, @sqlwatch_database_name = 'SQLWatch', @environment = 'Booking & Search', @linked_server_name = 'PRLCXWTSSSR06', @rmtuser = NULL, @rmtpassword = NULL
    GO

    EXEC [dbo].[usp_sqlwatch_user_repository_add_remote_instance] @sql_instance = 'PRLCXWTSSSR07', @hostname = 'PRLCXWTSSSR07', @sql_port = NULL, @sqlwatch_database_name = 'SQLWatch', @environment = 'Booking & Search', @linked_server_name = 'PRLCXWTSSSR07', @rmtuser = NULL, @rmtpassword = NULL
    GO

    EXEC [dbo].[usp_sqlwatch_user_repository_add_remote_instance] @sql_instance = 'PRLCXWTSSSR08', @hostname = 'PRLCXWTSSSR08', @sql_port = NULL, @sqlwatch_database_name = 'SQLWatch', @environment = 'Booking & Search', @linked_server_name = 'PRLCXWTSSSR08', @rmtuser = NULL, @rmtpassword = NULL
    GO

    EXEC [dbo].[usp_sqlwatch_user_repository_add_remote_instance] @sql_instance = 'PRLCXWTSSSR09', @hostname = 'PRLCXWTSSSR09', @sql_port = NULL, @sqlwatch_database_name = 'SQLWatch', @environment = 'Booking & Search', @linked_server_name = 'PRLCXWTSSSR09', @rmtuser = NULL, @rmtpassword = NULL

    EXEC [dbo].[usp_sqlwatch_user_repository_add_remote_instance] @sql_instance = 'TELAMWTSSQL01', @hostname = 'TELAMWTSSQL01', @sql_port = NULL, @sqlwatch_database_name = 'SQLWatch', @environment = 'UAT', @linked_server_name = 'TELAMWTSSQL01', @rmtuser = NULL, @rmtpassword = NULL

    EXEC [dbo].[usp_sqlwatch_user_repository_add_remote_instance] @sql_instance = 'TS-DBSRV-01\SQLDB1', @hostname = 'TS-DBSRV-01\SQLDB1', @sql_port = NULL, @sqlwatch_database_name = 'SQLWatch', @environment = 'Staging', @linked_server_name = 'TS-DBSRV-01\SQLDB1', @rmtuser = NULL, @rmtpassword = NULL

    EXEC [dbo].[usp_sqlwatch_user_repository_add_remote_instance] @sql_instance = 'TS-LISRV-01', @hostname = 'TS-LISRV-01', @sql_port = NULL, @sqlwatch_database_name = 'SQLWatch', @environment = 'Local Inventory', @linked_server_name = 'TS-LISRV-01', @rmtuser = NULL, @rmtpassword = NULL

    EXEC [dbo].[usp_sqlwatch_user_repository_add_remote_instance] @sql_instance = 'TS-LISRV-02', @hostname = 'TS-LISRV-02', @sql_port = NULL, @sqlwatch_database_name = 'SQLWatch', @environment = 'Local Inventory', @linked_server_name = 'TS-LISRV-02', @rmtuser = NULL, @rmtpassword = NULL

    EXEC [dbo].[usp_sqlwatch_user_repository_add_remote_instance] @sql_instance = 'TSSSAWS-101', @hostname = 'TSSSAWS-101', @sql_port = NULL, @sqlwatch_database_name = 'SQLWatch', @environment = 'AWS Search', @linked_server_name = 'TSSSAWS-101', @rmtuser = NULL, @rmtpassword = NULL

    EXEC [dbo].[usp_sqlwatch_user_repository_add_remote_instance] @sql_instance = 'TSSSAWS-104', @hostname = 'TSSSAWS-104', @sql_port = NULL, @sqlwatch_database_name = 'SQLWatch', @environment = 'AWS Search', @linked_server_name = 'TSSSAWS-104', @rmtuser = NULL, @rmtpassword = NULL

    EXEC [dbo].[usp_sqlwatch_user_repository_add_remote_instance] @sql_instance = 'TSSSAWS-105', @hostname = 'TSSSAWS-105', @sql_port = NULL, @sqlwatch_database_name = 'SQLWatch', @environment = 'AWS Search', @linked_server_name = 'TSSSAWS-105', @rmtuser = NULL, @rmtpassword = NULL

    EXEC [dbo].[usp_sqlwatch_user_repository_add_remote_instance] @sql_instance = 'TS-SSVR-01', @hostname = 'TS-SSVR-01', @sql_port = NULL, @sqlwatch_database_name = 'SQLWatch', @environment = 'Staging', @linked_server_name = 'TS-SSVR-01', @rmtuser = NULL, @rmtpassword = NULL

    EXEC [dbo].[usp_sqlwatch_user_repository_add_remote_instance] @sql_instance = 'TS-SSVR-02', @hostname = 'TS-SSVR-02', @sql_port = NULL, @sqlwatch_database_name = 'SQLWatch', @environment = 'Staging', @linked_server_name = 'TS-SSVR-02', @rmtuser = NULL, @rmtpassword = NULL

    EXEC [dbo].[usp_sqlwatch_user_repository_add_remote_instance] @sql_instance = 'TSSTGLIDB', @hostname = 'TSSTGLIDB', @sql_port = NULL, @sqlwatch_database_name = 'SQLWatch', @environment = 'Staging', @linked_server_name = 'TSSTGLIDB', @rmtuser = NULL, @rmtpassword = NULL

END