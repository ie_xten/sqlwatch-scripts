USE [master]
GO
CREATE LOGIN [EBIZ\svc_ts_p_ss_sql_agen] FROM WINDOWS WITH DEFAULT_DATABASE=[master]
GO
USE [SQLWatch]
GO
CREATE USER [EBIZ\svc_ts_p_ss_sql_agen] FOR LOGIN [EBIZ\svc_ts_p_ss_sql_agen]
GO
USE [SQLWatch]
GO
ALTER ROLE [db_datareader] ADD MEMBER [EBIZ\svc_ts_p_ss_sql_agen]
GO
