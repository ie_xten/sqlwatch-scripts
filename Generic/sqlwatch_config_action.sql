/*
https://docs.sqlwatch.io/v/2.2/alerts-and-reports/how-to/add-or-modify-action

*/

SET XACT_ABORT ON

USE [SQLWatch]
GO

DELETE FROM [dbo].[sqlwatch_config_action]
WHERE [action_id] > 0 AND [action_id] < 38
GO

--Insert custom actions

SET IDENTITY_INSERT [dbo].[sqlwatch_config_action] ON

DECLARE @SupportQuery VARCHAR(255)
DECLARE @LoggingQuery VARCHAR(255)
DECLARE @DbMailProfile VARCHAR(100)

SET @DbMailProfile = (SELECT TOP(1) [name] FROM [msdb].[dbo].[sysmail_profile])

SET @SupportQuery = 'exec msdb.dbo.sp_send_dbmail @recipients = ''ie@xTEN.uk'',
@subject = ''{SUBJECT}'',
@body = ''{BODY}'',
@profile_name=''' + ISNULL(@DbMailProfile,'') + ''',
@body_format = ''HTML'''

BEGIN TRAN

	UPDATE [dbo].[sqlwatch_config_action]
	SET [action_exec] = @SupportQuery,
	[action_enabled] = 1
	WHERE [action_id] = -1

COMMIT

SET @DbMailProfile = (SELECT TOP(1) [name] FROM [msdb].[dbo].[sysmail_profile])

SET @SupportQuery = 'exec msdb.dbo.sp_send_dbmail @recipients = ''ie@xTEN.uk'',
@subject = ''{SUBJECT}'',
@body = ''{BODY}'',
@profile_name=''' + ISNULL(@DbMailProfile,'') + ''''

BEGIN TRAN

	UPDATE [dbo].[sqlwatch_config_action]
	SET [action_exec] = @SupportQuery,
	[action_enabled] = 1
	WHERE [action_id] = -2

COMMIT

/*

IF NOT EXISTS(SELECT 1 FROM [dbo].[sqlwatch_config_action] WHERE [action_id] = 3)
BEGIN

INSERT INTO [dbo].[sqlwatch_config_action] ([action_id],[action_description],[action_exec_type],[action_exec],[action_report_id],[action_enabled])
VALUES (3,'Clock Skew report','T-SQL',NULL,3,0)
END
GO

IF NOT EXISTS(SELECT 1 FROM [dbo].[sqlwatch_config_action] WHERE [action_id] = 4)
BEGIN

INSERT INTO [dbo].[sqlwatch_config_action] ([action_id],[action_description],[action_exec_type],[action_exec],[action_report_id],[action_enabled])
VALUES (4,'Remote Data Collection is Failing report','T-SQL',NULL,4,0)
END
GO

IF NOT EXISTS(SELECT 1 FROM [dbo].[sqlwatch_config_action] WHERE [action_id] = 5)
BEGIN

INSERT INTO [dbo].[sqlwatch_config_action] ([action_id],[action_description],[action_exec_type],[action_exec],[action_report_id],[action_enabled])
VALUES (5,'Agent Job duration longer than expected report','T-SQL',NULL,5,0)
END
GO

IF NOT EXISTS(SELECT 1 FROM [dbo].[sqlwatch_config_action] WHERE [action_id] = 6)
BEGIN

INSERT INTO [dbo].[sqlwatch_config_action] ([action_id],[action_description],[action_exec_type],[action_exec],[action_report_id],[action_enabled])
VALUES (6,'Data I/O utilization % in last 10 minutes report','T-SQL',NULL,6,0)
END
GO

IF NOT EXISTS(SELECT 1 FROM [dbo].[sqlwatch_config_action] WHERE [action_id] = 7)
BEGIN

INSERT INTO [dbo].[sqlwatch_config_action] ([action_id],[action_description],[action_exec_type],[action_exec],[action_report_id],[action_enabled])
VALUES (7,'Database Free Space (MB) report','T-SQL',NULL,7,0)
END
GO


IF EXISTS (SELECT 1	FROM sys.system_objects	WHERE [name] LIKE 'sys.dm_db_resource_stats') --Azure Only
BEGIN
    IF NOT EXISTS(SELECT 1 FROM [dbo].[sqlwatch_config_action] WHERE [action_id] = 8)
    BEGIN

    INSERT INTO [dbo].[sqlwatch_config_action] ([action_id],[action_description],[action_exec_type],[action_exec],[action_report_id],[action_enabled])
        VALUES (8,'DTU utilization % in last 10 minutes report','T-SQL',NULL,8,0)
    END
END
GO

IF EXISTS (SELECT 1	FROM sys.system_objects	WHERE [name] LIKE 'sys.dm_db_resource_stats') --Azure Only
BEGIN
    IF NOT EXISTS(SELECT 1 FROM [dbo].[sqlwatch_config_action] WHERE [action_id] = 9)
    BEGIN

    INSERT INTO [dbo].[sqlwatch_config_action] ([action_id],[action_description],[action_exec_type],[action_exec],[action_report_id],[action_enabled])
        VALUES (9,'Log I/O utilization % in the last 10 minutes report','T-SQL',NULL,9,0)
    END
END
GO

IF EXISTS (SELECT 1	FROM sys.system_objects	WHERE [name] LIKE 'sys.dm_db_resource_stats') --Azure Only
BEGIN


    IF NOT EXISTS(SELECT 1 FROM [dbo].[sqlwatch_config_action] WHERE [action_id] = 10)
    BEGIN

    INSERT INTO [dbo].[sqlwatch_config_action] ([action_id],[action_description],[action_exec_type],[action_exec],[action_report_id],[action_enabled])
        VALUES (10,'Session limit % in the last 10 minutes report','T-SQL',NULL,10,0)
    END
END
GO

IF EXISTS (SELECT 1	FROM sys.system_objects	WHERE [name] LIKE 'sys.dm_db_resource_stats') --Azure Only
BEGIN
    IF NOT EXISTS(SELECT 1 FROM [dbo].[sqlwatch_config_action] WHERE [action_id] = 11)
    BEGIN

    INSERT INTO [dbo].[sqlwatch_config_action] ([action_id],[action_description],[action_exec_type],[action_exec],[action_report_id],[action_enabled])
        VALUES (11,'Worker thread limit % in the last 10 minutes report','T-SQL',NULL,11,0)
    END
END
GO

IF NOT EXISTS(SELECT 1 FROM [dbo].[sqlwatch_config_action] WHERE [action_id] = 12)
BEGIN

INSERT INTO [dbo].[sqlwatch_config_action] ([action_id],[action_description],[action_exec_type],[action_exec],[action_report_id],[action_enabled])
VALUES (12,'Cluster failover in the last 10 minutes report','T-SQL',NULL,12,0)
END
GO

IF NOT EXISTS(SELECT 1 FROM [dbo].[sqlwatch_config_action] WHERE [action_id] = 13)
BEGIN

INSERT INTO [dbo].[sqlwatch_config_action] ([action_id],[action_description],[action_exec_type],[action_exec],[action_report_id],[action_enabled])
VALUES (13,'Days until Database file is full report','T-SQL',NULL,13,0)
END
GO

IF NOT EXISTS(SELECT 1 FROM [dbo].[sqlwatch_config_action] WHERE [action_id] = 14)
BEGIN

INSERT INTO [dbo].[sqlwatch_config_action] ([action_id],[action_description],[action_exec_type],[action_exec],[action_report_id],[action_enabled])
VALUES (14,'Replication Commands report','T-SQL',NULL,14,0)
END
GO

IF NOT EXISTS(SELECT 1 FROM [dbo].[sqlwatch_config_action] WHERE [action_id] = 15)
BEGIN

INSERT INTO [dbo].[sqlwatch_config_action] ([action_id],[action_description],[action_exec_type],[action_exec],[action_report_id],[action_enabled])
VALUES (15,'Failed Logins in the past 60 Mins report','T-SQL',NULL,15,0)
END
GO

IF NOT EXISTS(SELECT 1 FROM [dbo].[sqlwatch_config_action] WHERE [action_id] = 16)
BEGIN

INSERT INTO [dbo].[sqlwatch_config_action] ([action_id],[action_description],[action_exec_type],[action_exec],[action_report_id],[action_enabled])
VALUES (16,'SQL Server Full Text Search Service Stopped report','T-SQL',NULL,16,0)
END
GO

IF NOT EXISTS(SELECT 1 FROM [dbo].[sqlwatch_config_action] WHERE [action_id] = 17)
BEGIN

INSERT INTO [dbo].[sqlwatch_config_action] ([action_id],[action_description],[action_exec_type],[action_exec],[action_report_id],[action_enabled])
VALUES (17,'SQL Server Integration Service Stopped report','T-SQL',NULL,17,0)
END
GO

IF NOT EXISTS(SELECT 1 FROM [dbo].[sqlwatch_config_action] WHERE [action_id] = 18)
BEGIN

INSERT INTO [dbo].[sqlwatch_config_action] ([action_id],[action_description],[action_exec_type],[action_exec],[action_report_id],[action_enabled])
VALUES (18,'SQL Server VSS Writer Service Stopped report','T-SQL',NULL,18,0)
END
GO

IF NOT EXISTS(SELECT 1 FROM [dbo].[sqlwatch_config_action] WHERE [action_id] = 19)
BEGIN

INSERT INTO [dbo].[sqlwatch_config_action] ([action_id],[action_description],[action_exec_type],[action_exec],[action_report_id],[action_enabled])
VALUES (19,'SQL Server Analysis Service Stopped report','T-SQL',NULL,19,0)
END
GO

IF NOT EXISTS(SELECT 1 FROM [dbo].[sqlwatch_config_action] WHERE [action_id] = 20)
BEGIN

INSERT INTO [dbo].[sqlwatch_config_action] ([action_id],[action_description],[action_exec_type],[action_exec],[action_report_id],[action_enabled])
VALUES (20,'SQL Server Reporting Service Stopped report','T-SQL',NULL,20,0)
END
GO

IF NOT EXISTS(SELECT 1 FROM [dbo].[sqlwatch_config_action] WHERE [action_id] = 21)
BEGIN

INSERT INTO [dbo].[sqlwatch_config_action] ([action_id],[action_description],[action_exec_type],[action_exec],[action_report_id],[action_enabled])
VALUES (21,'Availability group database not healthy report','T-SQL',NULL,21,0)
END
GO

IF NOT EXISTS(SELECT 1 FROM [dbo].[sqlwatch_config_action] WHERE [action_id] = 22)
BEGIN

INSERT INTO [dbo].[sqlwatch_config_action] ([action_id],[action_description],[action_exec_type],[action_exec],[action_report_id],[action_enabled])
VALUES (22,'Availability group failover report','T-SQL',NULL,22,0)
END
GO

IF NOT EXISTS(SELECT 1 FROM [dbo].[sqlwatch_config_action] WHERE [action_id] = 23)
BEGIN

INSERT INTO [dbo].[sqlwatch_config_action] ([action_id],[action_description],[action_exec_type],[action_exec],[action_report_id],[action_enabled])
VALUES (23,'Availability group listener offline report','T-SQL',NULL,23,0)
END
GO

IF NOT EXISTS(SELECT 1 FROM [dbo].[sqlwatch_config_action] WHERE [action_id] = 24)
BEGIN

INSERT INTO [dbo].[sqlwatch_config_action] ([action_id],[action_description],[action_exec_type],[action_exec],[action_report_id],[action_enabled])
VALUES (24,'Availability group not ready for failover report','T-SQL',NULL,24,0)
END
GO

IF NOT EXISTS(SELECT 1 FROM [dbo].[sqlwatch_config_action] WHERE [action_id] = 25)
BEGIN

INSERT INTO [dbo].[sqlwatch_config_action] ([action_id],[action_description],[action_exec_type],[action_exec],[action_report_id],[action_enabled])
VALUES (25,'Query slowdown due to availability group report','T-SQL',NULL,25,0)
END
GO

IF NOT EXISTS(SELECT 1 FROM [dbo].[sqlwatch_config_action] WHERE [action_id] = 26)
BEGIN

INSERT INTO [dbo].[sqlwatch_config_action] ([action_id],[action_description],[action_exec_type],[action_exec],[action_report_id],[action_enabled])
VALUES (26,'Availability group replica not healthy report','T-SQL',NULL,26,0)
END
GO

IF NOT EXISTS(SELECT 1 FROM [dbo].[sqlwatch_config_action] WHERE [action_id] = 27)
BEGIN

INSERT INTO [dbo].[sqlwatch_config_action] ([action_id],[action_description],[action_exec_type],[action_exec],[action_report_id],[action_enabled])
VALUES (27,'Availability group replication falling behind report','T-SQL',NULL,27,0)
END
GO

SET IDENTITY_INSERT [dbo].[sqlwatch_config_action] OFF;
GO

*/