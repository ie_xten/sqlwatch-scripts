﻿<#
.SYNOPSIS
 Installs SQLWATCH on specified servers via dacpac or DBAtools

.DESCRIPTION
 Installs SQLWATCH on specified servers via dacpac or DBAtools. Make sure to uncomment servers for each customer. If schema changes are made during the release, install using DACPAC to take advantage of the Add /p:blockonPossibleDaatLoss=False with flag 

 .Flags ServerListADCred
 Serevrs in this array will be installed using Active Director credentials

 .Flags ServerListADCred
 Servers in this array will be installed using SQL Server credentials

 #requires -version 3

 #>

#---------------------------------------------------------[Initialisations]--------------------------------------------------------

$ErrorActionPreference = "Continue"


#----------------------------------------------------------[Declarations]----------------------------------------------------------

# If 1, use sqlpackage.exe and dacpac. If 0, use DBATools. Only need to use DACPac if schema changes have occured and data would be lossed, or if you're deploying a version which is not yet resleased on DBATools.
[bool]$InstallUsingDacpac = 1 

# Name of the database SQLWATCH will be deployed to
[string]$TargetDatabase = "SQLWatch"

# Location where scripts are deployed
[string]$WorkingDirectory = "C:\SQLWatch"

# List of SQL server instance which will be deployed to via AD login
[array]$ServerListADCred = (Get-ChildItem $WorkingDirectory -Include ServerListADCred.csv -Recurse | Select-Object -ExpandProperty FullName | Import-CSV -Header Server)

# List of SQL server instance which will be deployed to via SQL credentials. If not empty, powershell will propt you to enter credentials
[array]$ServerListSQLCred = (Get-ChildItem $WorkingDirectory -Include ServerListSQLCred.csv -Recurse | Select-Object -ExpandProperty FullName | Import-CSV -Header Server)

# Location of the dacpacs used during the deployment
[string]$SQLWATCHdacpac = "$WorkingDirectory\SQLWATCH.dacpac"
[string]$masterdacpac = "$WorkingDirectory\master.dacpac"
[string]$msdbdacpac = "$WorkingDirectory\msdb.dacpac"

# Location of the XML DAC Profile for Sqlpackage.exe. See https://docs.microsoft.com/en-us/sql/tools/sqlpackage?view=sql-server-ver15 for potentical parameters
[string]$DACProfile = "$WorkingDirectory\SQLWATCH.publish.xml"

# Location where the error log will be written to within the Write-Log function.
[string]$LogPath = "$WorkingDirectory\DeploySQLWatch_ERRORS_" + (Get-Date -Format yyyy_MM_dd_HH_mm_ss) + ".log"

# Location of the post deployment scripts. Each script deploys serially and independently of wetehr the previsou script successed or failed
[array]$PostDeploymentScripts = (Get-ChildItem $WorkingDirectory -Include *.SQL -Recurse | Select-Object -ExpandProperty FullName)

# SQLWATCH will be default deploy in parerrel. Specify the number of threads here.
[int]$maxThreads = 5

[string]$SqlPackageLocation = "" # Populated later
# [securestring]$Credential = "" # Populated later


#-----------------------------------------------------------[Pre-Execution Validation]------------------------------------------------------------

# The following section checks all prerequisites exist

IF ($InstallUSingDacpac -eq $True) {
    IF ((Test-Path $SQLWATCHdacpac) -eq $False) {
        Write-host "Cannot find specified file $SQLWATCHdacpac Check the file location or install using DBATools. Terminating..." -ForegroundColor Yellow -BackgroundColor Black
        BREAK
    }
    IF ((Test-Path $masterdacpac) -eq $False) {
        Write-host "Cannot find specified file $masterdacpac Check the file location or install using DBATools. Terminating..." -ForegroundColor Yellow -BackgroundColor Black
        BREAK
    }
    IF ((Test-Path $msdbdacpac) -eq $False) {
        Write-host "Cannot find specified file $msdbdacpac Check the file location or install using DBATools. Terminating..." -ForegroundColor Yellow -BackgroundColor Black
        BREAK
    }
    $SqlPackageLocation = (Get-ChildItem "C:\Program Files (x86)\Microsoft SQL Server" -filter "SqlPackage.exe" -Recurse | Select-Object -First 1).FullName
    IF ((Test-Path "$SqlPackageLocation") -eq $False) {
        Write-host "Cannot find SqlPackage.exe in C:\Program Files (x86)\Microsoft SQL Server. Check the file location or install using DBATools. Terminating..." -ForegroundColor Yellow -BackgroundColor Black
        BREAK
    }
}
IF (($ServerListADCred.Count -eq 0) -and ($ServerListSQLCred.Count -eq 0)) {
    Write-host "List of servers to be deployed to cannot be zero. Terminating..." -ForegroundColor Yellow -BackgroundColor Black
    BREAK
}
IF (($InstallUSingDacpac -eq $False) -or ($ServerListSQLCred.Count -gt 0)) {
    IF (Get-Module -ListAvailable -Name "dbatools") {
        Write-Host "Installing SQLWatch via DBATools"
    } 
    ELSE {
        Write-Host "DBATools is required for servers using SQLCredentials. Download using `"Install-Module dbatools`" or install manually on `$ServerListSQLCred servers"
    }
}
# No longer required but needs to be tested.
# $SQLCMD = (Get-ChildItem "C:\Program Files (x86)\Microsoft SQL Server" -filter "SQLCMD.EXE" -Recurse | Select-Object -First 1).FullName
<# IF ((Test-Path "$SQLCMD") -eq $False) {
    Write-host "Cannot find SQLCMD.EXE in C:\Program Files (x86)\Microsoft SQL Server. Check the file location or install using DBATools. Terminating..." -ForegroundColor Yellow -BackgroundColor Black
    BREAK
} #>
IF ($ServerListSQLCred.Count -gt 0) {
    #$Credential = Get-Credential -Message 'Input Credentials for SQL login'
    Write-host "Cannot deploy to SQLCred Servers currently. Terminating..." -ForegroundColor Yellow -BackgroundColor Black
    BREAK
}


#-----------------------------------------------------------[Execution]------------------------------------------------------------

# Deploys SQLWATCH daatabase, runs the jobs and executed post deployment scripts
Workflow Publish-SQLWatch {

    PARAM 
    ( 
        [Parameter(Mandatory=$true, ValueFromPipelineByPropertyName=$true)]
        [bool]$InstallUsingDacpac,
        [string]$TargetDatabase,
        [string]$WorkingDirectory,
        [array]$ServerListADCred,
        [array]$ServerListSQLCred,
        [string]$SQLWATCHdacpac,
        [string]$masterdacpac,
        [string]$msdbdacpac,
        [string]$DACProfile,
        [string]$LogPath,
        [array]$PostDeploymentScripts,
        [string]$SqlPackageLocation,
        [securestring]$Credential,
        [int]$maxThreads
    )

    function Write-Log 
    { 
        [CmdletBinding()] 
        Param 
        ( 
            [Parameter(Mandatory=$true, 
                       ValueFromPipelineByPropertyName=$true)] 
            [ValidateNotNullOrEmpty()] 
            [Alias("LogContent")] 
            [string]$Message, 
 
            [Parameter(Mandatory=$false)] 
            [Alias('LogPath')] 
            [string]$Path='C:\Logs\PowerShellLog.log', 
         
            [Parameter(Mandatory=$false)] 
            [ValidateSet("Error","Warn","Info")] 
            [string]$Level="Error", 
         
            [Parameter(Mandatory=$false)] 
            [switch]$NoClobber 
        ) 
 
        Begin 
        { 
            # Set VerbosePreference to Continue so that verbose messages are displayed. 
            $VerbosePreference = 'Continue' 
        } 
        Process 
        { 
         
            # If the file already exists and NoClobber was specified, do not write to the log. 
            if ((Test-Path $Path) -AND $NoClobber) { 
                Write-Error "Log file $Path already exists, and you specified NoClobber. Either delete the file or specify a different name." 
                Return 
                } 
 
            # If attempting to write to a log file in a folder/path that doesn't exist create the file including the path. 
            elseif (!(Test-Path $Path)) { 
                Write-Verbose "Creating $Path." 
                New-Item $Path -Force -ItemType File 
                } 
 
            else { 
                # Nothing to see here yet. 
                } 
 
            # Format Date for our Log File 
            $FormattedDate = Get-Date -Format "yyyy-MM-dd HH:mm:ss" 
 
            # Write message to error, warning, or verbose pipeline and specify $LevelText 
            switch ($Level) { 
                'Error' { 
                    Write-Error $Message 
                    $LevelText = 'ERROR:' 
                    } 
                'Warn' { 
                    Write-Warning $Message 
                    $LevelText = 'WARNING:' 
                    } 
                'Info' { 
                    Write-Verbose $Message 
                    $LevelText = 'INFO:' 
                    } 
                } 
         
            # Write log entry to $Path 
            "$FormattedDate $LevelText $Message" | Out-File -FilePath $Path -Append 
        } 
        End 
        { 
        } 
    }

    # Deploy SQLWATCH to all servers in the $ServerListADCred in parrallel
    FOREACH -parallel -throttlelimit $maxThreads ($Server in $ServerListADCred.Server) {
       Write-Log -Path $LogPath -Message "Deploying SQLWatch to $Server" -Level 'Info'

        # Install using sqlpackage.exe and dacpacfiles
        IF ($InstallUsingDacpac) {
            
            Try {
                Write-Log -Path $LogPath -Message "Deploying SQLWATCH to $Using:Server using $Using:SQLWATCHdacpac" -Level 'Info'
                InlineScript {
                    & $Using:SqlPackageLocation /Action:Publish /SourceFile:$Using:SQLWATCHdacpac /TargetDatabaseName:$Using:TargetDatabase /Profile:$Using:DACProfile /TargetServerName:$Using:Server
                }
            }
            Catch {
                    $ErrorMessage = $_
                    Write-Log -Path $LogPath -Message "$ErrorMessage" -Level 'Error'
            }
        }

        # Install using dba-tools
        ELSE {
            Try {
                Write-Log -Path $LogPath -Message "Deploying SQLWATCH to $Server using Dbatools" -Level 'Info'
                InlineScript {
                    Install-DbaSqlWatch -SqlInstance $Server -Database $TargetDatabase
                }
            }
            Catch {
                    $ErrorMessage = $_
                    Write-Log -Path $LogPath -Message "$ErrorMessage" -Level 'Error'
            }
        }

        # Run the post deployment .sql scripts
        IF ($LASTEXITCODE -ne 0) {
            FOREACH ($Script in $PostDeploymentScripts) {
                Try {
                    Write-Log -Path $LogPath -Message "Executing $Using:Script on $Using:Server" -Level 'Info'
                    InlineScript {
                        Invoke-Sqlcmd -ServerInstance $Using:Server -Database $Using:TargetDatabase -InputFile $Using:Script  -QueryTimeout 600 -AbortOnError -Verbose
                    }
                }
                Catch {
                    $ErrorMessage = $_
                    Write-Log -Path $LogPath -Message "$ErrorMessage" -Level 'Error'
                }
            }
        }
    }

    # Deploy SQLWATCH to all servers in the $ServerListSQLCred in parrallel
    FOREACH -parallel -throttlelimit $maxThreads ($Server in $ServerListSQLCred.Server) {
       Write-Log -Path $LogPath -Message "Deploying SQLWatch to $Server" -Level 'Info'

        Try {
            Write-Log -Path $LogPath -Message "Deploying SQLWATCH to $Server using Dbatools" -Level 'Info'
            InlineScript {
                Publish-DbaDacPackage -SqlInstance $Server -Database $TargetDatabase -Path $SQLWATCHdacpac -PublishXml $DACProfile -SqlCredential $Credential
            }
        }
        Catch {
                $ErrorMessage = $_
                Write-Log -Path $LogPath -Message "$ErrorMessage" -Level 'Error'
        }

        # Run the post deployment .sql scripts
        FOREACH ($Script in $PostDeploymentScripts) {
            Try {
                Write-Log -Path $LogPath -Message "Executing $Using:Script on $Using:Server" -Level 'Info'
                InlineScript {
                    Invoke-Sqlcmd -ServerInstance $Using:Server -Database $Using:TargetDatabase -InputFile $Using:Script  -QueryTimeout 600 -AbortOnError -Verbose
                }
            }
            Catch {
                    $ErrorMessage = $_
                    Write-Log -Path $LogPath -Message "$ErrorMessage" -Level 'Error'
            }
        }
    }
}

Publish-SQLWatch $InstallUsingDacpac $TargetDatabase $WorkingDirectory $ServerListADCred $ServerListSQLCred $SQLWATCHdacpac $masterdacpac $msdbdacpac $DACProfile $LogPath $PostDeploymentScripts $SqlPackageLocation $Credential $maxThreads