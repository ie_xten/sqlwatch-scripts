/*

SQLWATCH installs jobs with thte current AD account which created the jobs. We should run this script to change the sql watch jobs to sa

*/
USE [msdb]
GO

DECLARE @SA NVARCHAR(255) = ( SELECT [name] FROM sys.server_principals WHERE [sid] = 0x01 )

IF EXISTS(SELECT 1 FROM msdb..sysjobs WHERE [name] = 'SQLWATCH-INTERNAL-ACTIONS')
BEGIN

EXEC msdb.dbo.sp_update_job @job_name=N'SQLWATCH-INTERNAL-ACTIONS', 
		@owner_login_name=@SA
END
GO

DECLARE @SA NVARCHAR(255) = ( SELECT [name] FROM sys.server_principals WHERE [sid] = 0x01 )

IF EXISTS(SELECT 1 FROM msdb..sysjobs WHERE [name] = 'SQLWATCH-INTERNAL-CHECKS')
BEGIN

EXEC msdb.dbo.sp_update_job @job_name=N'SQLWATCH-INTERNAL-CHECKS', 
		@owner_login_name=@SA
END
GO

DECLARE @SA NVARCHAR(255) = ( SELECT [name] FROM sys.server_principals WHERE [sid] = 0x01 )

IF EXISTS(SELECT 1 FROM msdb..sysjobs WHERE [name] = 'SQLWATCH-INTERNAL-CONFIG')
BEGIN

EXEC msdb.dbo.sp_update_job @job_name=N'SQLWATCH-INTERNAL-CONFIG', 
		@owner_login_name=@SA
END
GO

DECLARE @SA NVARCHAR(255) = ( SELECT [name] FROM sys.server_principals WHERE [sid] = 0x01 )

IF EXISTS(SELECT 1 FROM msdb..sysjobs WHERE [name] = 'SQLWATCH-INTERNAL-RETENTION')
BEGIN

EXEC msdb.dbo.sp_update_job @job_name=N'SQLWATCH-INTERNAL-RETENTION',
		@owner_login_name=@SA
END
GO

DECLARE @SA NVARCHAR(255) = ( SELECT [name] FROM sys.server_principals WHERE [sid] = 0x01 )

IF EXISTS(SELECT 1 FROM msdb..sysjobs WHERE [name] = 'SQLWATCH-INTERNAL-TRENDS')
BEGIN

EXEC msdb.dbo.sp_update_job @job_name=N'SQLWATCH-INTERNAL-TRENDS',
		@owner_login_name=@SA
END
GO

DECLARE @SA NVARCHAR(255) = ( SELECT [name] FROM sys.server_principals WHERE [sid] = 0x01 )

IF EXISTS(SELECT 1 FROM msdb..sysjobs WHERE [name] = 'SQLWATCH-LOGGER-AGENT-HISTORY')
BEGIN

EXEC msdb.dbo.sp_update_job @job_name=N'SQLWATCH-LOGGER-AGENT-HISTORY', 
		@owner_login_name=@SA
END
GO

DECLARE @SA NVARCHAR(255) = ( SELECT [name] FROM sys.server_principals WHERE [sid] = 0x01 )

IF EXISTS(SELECT 1 FROM msdb..sysjobs WHERE [name] = 'SQLWATCH-LOGGER-DISK-UTILISATION')
BEGIN

EXEC msdb.dbo.sp_update_job @job_name=N'SQLWATCH-LOGGER-DISK-UTILISATION', 
		@owner_login_name=@SA
END
GO

DECLARE @SA NVARCHAR(255) = ( SELECT [name] FROM sys.server_principals WHERE [sid] = 0x01 )

IF EXISTS(SELECT 1 FROM msdb..sysjobs WHERE [name] = 'SQLWATCH-LOGGER-INDEXES')
BEGIN

EXEC msdb.dbo.sp_update_job @job_name=N'SQLWATCH-LOGGER-INDEXES', 
		@owner_login_name=@SA
END
GO

DECLARE @SA NVARCHAR(255) = ( SELECT [name] FROM sys.server_principals WHERE [sid] = 0x01 )

IF EXISTS(SELECT 1 FROM msdb..sysjobs WHERE [name] = 'SQLWATCH-LOGGER-PERFORMANCE')
BEGIN

EXEC msdb.dbo.sp_update_job @job_name=N'SQLWATCH-LOGGER-PERFORMANCE', 
		@owner_login_name=@SA
END
GO

DECLARE @SA NVARCHAR(255) = ( SELECT [name] FROM sys.server_principals WHERE [sid] = 0x01 )

IF EXISTS(SELECT 1 FROM msdb..sysjobs WHERE [name] = 'SQLWATCH-LOGGER-WHOISACTIVE')
BEGIN

EXEC msdb.dbo.sp_update_job @job_name=N'SQLWATCH-LOGGER-WHOISACTIVE', 
		@owner_login_name=@SA
END
GO

DECLARE @SA NVARCHAR(255) = ( SELECT [name] FROM sys.server_principals WHERE [sid] = 0x01 )

IF EXISTS(SELECT 1 FROM msdb..sysjobs WHERE [name] = 'SQLWATCH-INTERNAL-META-CONFIG')
BEGIN

EXEC msdb.dbo.sp_update_job @job_name=N'SQLWATCH-INTERNAL-META-CONFIG', 
		@owner_login_name=@SA
END
GO

