USE [SQLWatch]
GO

IF NOT EXISTS(SELECT 1 FROM [dbo].[sqlwatch_config_exclude_database] WHERE [database_name_pattern] = 'tempdb' and [snapshot_type_id] = 14)
BEGIN

INSERT INTO [sqlwatch].[dbo].[sqlwatch_config_exclude_database] ([database_name_pattern], [snapshot_type_id])
VALUES ('tempdb', 14)

END
GO

IF NOT EXISTS(SELECT 1 FROM [dbo].[sqlwatch_config_exclude_database] WHERE [database_name_pattern] = 'tempdb' and [snapshot_type_id] = 15)
BEGIN


INSERT INTO [sqlwatch].[dbo].[sqlwatch_config_exclude_database] ([database_name_pattern], [snapshot_type_id])
VALUES ('tempdb', 15)

END
GO