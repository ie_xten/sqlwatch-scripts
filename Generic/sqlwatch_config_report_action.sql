
/*

UPDATE rep_act
SET rep_act.[action_id] = 1
FROM [dbo].[sqlwatch_config_report_action] rep_act
INNER JOIN [dbo].[sqlwatch_config_report] rep ON rep.report_id = rep_act.report_id
INNER JOIN [dbo].[sqlwatch_config_action] act ON act.action_report_id = rep.report_id
INNER JOIN [dbo].[sqlwatch_config_check_action] che_act ON che_act.action_id = act.action_id
INNER JOIN [dbo].[sqlwatch_config_check] che ON che.check_id = che_act.check_id
WHERE che.check_name IN ('Long Running Open Transactions',
'Databases with no LOG backup',
'Databases with no DATA backup',
'Oldest DATA backup (days)',
'Oldest LOG backup (minutes)',
'Database page_verify not CHECKSUM',
'Databases not MULTI_USER',
'Databases not ONLINE',
'Databases with Auto Shrink Enabled',
'Databases with Auto Close Enabled',
'Queued actions have not been processed',
'Checks are failling',
'Check execution time is high',
'One or more disk will be full soon',
'Disk Free % is low',
'Action queue failure rate is high',
'Action queue is high',
'SQL Server Uptime is low',
'CPU Utilistaion',
'Blocking detected',
'Agent Job failure')
GO

UPDATE rep_act
SET rep_act.[action_id] = 2
FROM [dbo].[sqlwatch_config_report_action] rep_act
WHERE rep_act.[action_id] = -1
GO

*/

/*
SELECT che.[check_name],rep.[report_title]
FROM [dbo].[sqlwatch_config_report_action] rep_act
INNER JOIN [dbo].[sqlwatch_config_report] rep ON rep.report_id = rep_act.report_id
INNER JOIN [dbo].[sqlwatch_config_action] act ON act.action_report_id = rep.report_id
INNER JOIN [dbo].[sqlwatch_config_check_action] che_act ON che_act.action_id = act.action_id
INNER JOIN [dbo].[sqlwatch_config_check] che ON che.check_id = che_act.check_id
*/
