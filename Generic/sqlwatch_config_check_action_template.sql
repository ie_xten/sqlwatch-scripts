/*

Updates [sqlwatch_config_check_action_template] to standadise the template

First version tested on: 2.2.7325.22235
Most recently tested on: 2.2.7325.22235

*/
SET XACT_ABORT ON

USE [SQLWatch]
GO

BEGIN TRAN

	UPDATE [dbo].[sqlwatch_config_check_action_template]
	SET [action_template_fail_subject] = '{CHECK_NAME} on {SQL_INSTANCE}',
	[action_template_repeat_subject] = '{CHECK_NAME} on {SQL_INSTANCE}',
	[action_template_recover_subject] = 'ENDED: {CHECK_NAME} on {SQL_INSTANCE}'
	WHERE [action_template_id] BETWEEN -5 and 0

COMMIT

