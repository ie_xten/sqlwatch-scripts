/*
https://docs.sqlwatch.io/v/2.2/alerts-and-reports/how-to/get-notified-when-disk-has-low-free-space#add-a-new-check

*/
/*
SET XACT_ABORT ON

USE [SQLWatch]
GO

DELETE FROM [dbo].[sqlwatch_config_check]
WHERE [check_id] > 2 AND [check_id] < 37
GO

SET IDENTITY_INSERT [dbo].[sqlwatch_config_check] ON

--Insert custom checks

INSERT INTO [dbo].[sqlwatch_config_check] ([check_id], [check_name], [check_description], [check_query], [check_frequency_minutes], [check_threshold_warning], [check_threshold_critical], [check_enabled], [ignore_flapping])
VALUES (3,'Clock Skew','Time off last check on the remote instance differs from the time on local instance','',1,NULL,'>59',0,0);
GO

INSERT INTO [dbo].[sqlwatch_config_check] ([check_id], [check_name], [check_description], [check_query], [check_frequency_minutes], [check_threshold_warning], [check_threshold_critical], [check_enabled], [ignore_flapping])
VALUES (4,'Remote Data Collection is Failing','SQLWATCH has not collected data from a remote instances','SELECT COUNT(1)
FROM [dbo].[sqlwatch_logger_check] log_che
INNER JOIN [sqlwatch_config_sql_instance] sql_ins ON sql_ins.[sql_instance] = log_che.[sql_instance]
WHERE sql_ins.[collection_status] != ''Success''
AND sql_ins.[collection_status] IS NOT NULL
AND sql_ins.[is_active] = 1',5,NULL,'>1',0,0);  
GO

INSERT INTO [dbo].[sqlwatch_config_check] ([check_id], [check_name], [check_description], [check_query], [check_frequency_minutes], [check_threshold_warning], [check_threshold_critical], [check_enabled], [ignore_flapping])
VALUES (5,'Agent Job duration longer than expected','Agent Job duration longer than expected','',5,NULL,'>3',0,0);
GO

INSERT INTO [dbo].[sqlwatch_config_check] ([check_id], [check_name], [check_description], [check_query], [check_frequency_minutes], [check_threshold_warning], [check_threshold_critical], [check_enabled], [ignore_flapping])
VALUES (6,'Data I/O utilization % in last 10 minutes','In the past 10 minutes, Data I/O utilization has been higher than expected','',2,'>70','>90',0,0);
GO

INSERT INTO [dbo].[sqlwatch_config_check] ([check_id], [check_name], [check_description], [check_query], [check_frequency_minutes], [check_threshold_warning], [check_threshold_critical], [check_enabled], [ignore_flapping])
VALUES (7,'Database Free Space <5% (MB)','','SELECT COUNT(1)
FROM sys.database_files
WHERE type_desc = ''ROWS''
      AND (((size / 128.0) - (CAST(FILEPROPERTY(name, ''SpaceUsed'') AS INT) / 128.0)) / (size / 128.0)) * 100 < 5.0;'
,1,NULL,'>0',0,0);
GO

IF EXISTS (SELECT 1	FROM sys.system_objects	WHERE [name] LIKE 'sys.dm_db_resource_stats') --Azure Only
BEGIN
	INSERT INTO [dbo].[sqlwatch_config_check] ([check_id], [check_name], [check_description], [check_query], [check_frequency_minutes], [check_threshold_warning], [check_threshold_critical], [check_enabled], [ignore_flapping])
	VALUES (8, 'DTU utilization % in last 10 minutes', '', '', 2, '>70', '>90', 0, 0)
END
GO

IF EXISTS (SELECT 1	FROM sys.system_objects	WHERE [name] LIKE 'sys.dm_db_resource_stats') --Azure Only
BEGIN
    INSERT INTO [dbo].[sqlwatch_config_check] ([check_id], [check_name], [check_description], [check_query], [check_frequency_minutes], [check_threshold_warning], [check_threshold_critical], [check_enabled], [ignore_flapping])
    VALUES (9,'Log I/O utilization % in the last 10 minutes','','',2,'>70','>90',0,0)
END
GO

IF EXISTS (SELECT 1	FROM sys.system_objects	WHERE [name] LIKE 'sys.dm_db_resource_stats') --Azure Only
BEGIN
    INSERT INTO [dbo].[sqlwatch_config_check] ([check_id], [check_name], [check_description], [check_query], [check_frequency_minutes], [check_threshold_warning], [check_threshold_critical], [check_enabled], [ignore_flapping])
    VALUES (10,'Session limit % in the last 10 minutes','','',2,'>90','>95',0,0)
END
GO

IF EXISTS (SELECT 1	FROM sys.system_objects	WHERE [name] LIKE 'sys.dm_db_resource_stats') --Azure Only
BEGIN
    INSERT INTO [dbo].[sqlwatch_config_check] ([check_id], [check_name], [check_description], [check_query], [check_frequency_minutes], [check_threshold_warning], [check_threshold_critical], [check_enabled], [ignore_flapping])
    VALUES (11,'Worker thread limit % in the last 10 minutes','','',2,'>90','>95',0,0)
END
GO

INSERT INTO [dbo].[sqlwatch_config_check] ([check_id], [check_name], [check_description], [check_query], [check_frequency_minutes], [check_threshold_warning], [check_threshold_critical], [check_enabled], [ignore_flapping])
VALUES (12,'IO Stalls detected','','SELECT MAX([io_stall] / ([num_of_reads] + [num_of_writes]))
FROM (
	SELECT LEFT(UPPER(mf.physical_name), 2) AS Drive, SUM(num_of_reads) AS num_of_reads, SUM(io_stall_read_ms) AS io_stall_read_ms, SUM(num_of_writes) AS num_of_writes, SUM(io_stall_write_ms) AS io_stall_write_ms, SUM(num_of_bytes_read) AS num_of_bytes_read, SUM(num_of_bytes_written) AS num_of_bytes_written, SUM(io_stall) AS io_stall, vs.volume_mount_point, vfs.file_id
	FROM sys.dm_io_virtual_file_stats(NULL, NULL) AS vfs
	INNER JOIN sys.master_files AS mf WITH (NOLOCK) ON vfs.database_id = mf.database_id
		AND vfs.file_id = mf.file_id
	CROSS APPLY sys.dm_os_volume_stats(mf.database_id, mf.[file_id]) AS vs
	GROUP BY LEFT(UPPER(mf.physical_name), 2), vs.volume_mount_point, vfs.file_id
	) AS tab
WHERE tab.file_id <> 2',60,NULL,'>0',0,0);
GO

INSERT INTO [dbo].[sqlwatch_config_check] ([check_id], [check_name], [check_description], [check_query], [check_frequency_minutes], [check_threshold_warning], [check_threshold_critical], [check_enabled], [ignore_flapping])
VALUES (13,'Days until Database file is full','','',720,'<90','<30',0,0);
GO

INSERT INTO [dbo].[sqlwatch_config_check] ([check_id], [check_name], [check_description], [check_query], [check_frequency_minutes], [check_threshold_warning], [check_threshold_critical], [check_enabled], [ignore_flapping])
VALUES (14,'Replication Commands','Number of Transactional replication commands awaiting distribution','SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED;

USE [distribution]

SELECT TOP 1 COUNT(1) AS Commands
FROM MSrepl_commands cmds
    INNER JOIN MSarticles a
        ON a.article_id = cmds.article_id
    INNER JOIN MSrepl_transactions t
        ON cmds.xact_seqno = t.xact_seqno
GROUP BY CONVERT(SMALLDATETIME, t.entry_time)
ORDER BY CONVERT(SMALLDATETIME, t.entry_time) DESC;',10,NULL,'>10000',0,0);
GO

INSERT INTO [dbo].[sqlwatch_config_check] ([check_id], [check_name], [check_description], [check_query], [check_frequency_minutes], [check_threshold_warning], [check_threshold_critical], [check_enabled], [ignore_flapping])
VALUES (15,'Failed Logins in the past 60 Mins','','DECLARE @endtime DATETIME;
DECLARE @out DECIMAL(28, 2)

SET @endtime = GETDATE();

DECLARE @starttime DATETIME;

SET @starttime = DATEADD(hh, -1, @endtime);

IF OBJECT_ID(''tempdb..#LogEntries'') IS NOT NULL
	DROP TABLE #LogEntries;

CREATE TABLE #LogEntries (
	LogDate DATETIME,
	ProcessInfo VARCHAR(1000),
	LogMessage TEXT
	);

INSERT INTO #LogEntries
EXEC sys.xp_readerrorlog 0,
	1,
	N''Login'',
	N''failed'',
	@starttime,
	@endtime;

SELECT @output = SELECT LogDate,
ProcessInfo,
LogMessage
FROM #LogEntries;

SELECT @output',59,NULL,'>0',0,0);
GO

INSERT INTO [dbo].[sqlwatch_config_check] ([check_id], [check_name], [check_description], [check_query], [check_frequency_minutes], [check_threshold_warning], [check_threshold_critical], [check_enabled], [ignore_flapping])
VALUES (16,'SQL Server Full Text Search Service Stopped','','SELECT CASE 
    WHEN dss.servicename LIKE ''SQL Full-text Filter Daemon Launcher %'' AND dss.startup_type_desc = ''Automatic'' AND dss.status_desc <> ''Running'' THEN 0
    ELSE 1
    END
    AS ServiceStatusOK
FROM msdb.sys.dm_server_services dss
WHERE dss.servicename LIKE ''SQL Full-text Filter Daemon Launcher %'';',1,NULL,'>0',0,0);
GO

INSERT INTO [dbo].[sqlwatch_config_check] ([check_id], [check_name], [check_description], [check_query], [check_frequency_minutes], [check_threshold_warning], [check_threshold_critical], [check_enabled], [ignore_flapping])
VALUES (17,'SQL Server Integration Service Stopped','','',1,NULL,'>0',0,0);
GO

INSERT INTO [dbo].[sqlwatch_config_check] ([check_id], [check_name], [check_description], [check_query], [check_frequency_minutes], [check_threshold_warning], [check_threshold_critical], [check_enabled], [ignore_flapping])
VALUES (18,'SQL Server VSS Writer Service Stopped','','',1,NULL,'>0',0,0);
GO

INSERT INTO [dbo].[sqlwatch_config_check] ([check_id], [check_name], [check_description], [check_query], [check_frequency_minutes], [check_threshold_warning], [check_threshold_critical], [check_enabled], [ignore_flapping])
VALUES (19,'SQL Server Analysis Service Stopped','','',1,NULL,'>0',0,0);
GO

INSERT INTO [dbo].[sqlwatch_config_check] ([check_id], [check_name], [check_description], [check_query], [check_frequency_minutes], [check_threshold_warning], [check_threshold_critical], [check_enabled], [ignore_flapping])
VALUES (20,'SQL Server Reporting Service Stopped','','',1,NULL,'>0',0,0);
GO

INSERT INTO [dbo].[sqlwatch_config_check] ([check_id], [check_name], [check_description], [check_query], [check_frequency_minutes], [check_threshold_warning], [check_threshold_critical], [check_enabled], [ignore_flapping])
VALUES (21,'Availability group database not healthy','','SELECT COUNT(1) FROM [sys].[dm_hadr_database_replica_states] WHERE [synchronization_health] != 2',1,NULL,'>0',0,0);
GO

INSERT INTO [dbo].[sqlwatch_config_check] ([check_id], [check_name], [check_description], [check_query], [check_frequency_minutes], [check_threshold_warning], [check_threshold_critical], [check_enabled], [ignore_flapping])
VALUES (22,'Availability group failover','Checks wether an Always on Availability group failover has occured in the last hour','WITH cte_HADR AS (SELECT object_name, CONVERT(XML, event_data) AS data
FROM sys.fn_xe_file_target_read_file(''AlwaysOn*.xel'', null, null, null)
WHERE object_name = ''error_reported''
)

SELECT COUNT(1)
FROM cte_HADR
WHERE data.value(''(/event/data[@name=''''error_number''''])[1]'',''int'') = 1480
AND data.value(''(/event/@timestamp)[1]'',''datetime'') > CONVERT(DATE, DATEADD(HOUR, -1, GETDATE()))',1,NULL,'>0',0,0);
GO

INSERT INTO [dbo].[sqlwatch_config_check] ([check_id], [check_name], [check_description], [check_query], [check_frequency_minutes], [check_threshold_warning], [check_threshold_critical], [check_enabled], [ignore_flapping])
VALUES (23,'Availability group listener offline','','SELECT COUNT(1) FROM sys.availability_group_listener_ip_addresses aglia INNER JOIN sys.availability_group_listeners agl ON agl.listener_id = aglia.listener_id
WHERE aglia.[state] != 1',1,NULL,'>0',0,0);
GO

INSERT INTO [dbo].[sqlwatch_config_check] ([check_id], [check_name], [check_description], [check_query], [check_frequency_minutes], [check_threshold_warning], [check_threshold_critical], [check_enabled], [ignore_flapping])
VALUES (24,'Availability group not ready for failover','','SELECT is_failover_ready FROM sys.dm_hadr_database_replica_cluster_states WHERE is_failover_ready != 1',1,NULL,'>0',0,0);
GO

INSERT INTO [dbo].[sqlwatch_config_check] ([check_id], [check_name], [check_description], [check_query], [check_frequency_minutes], [check_threshold_warning], [check_threshold_critical], [check_enabled], [ignore_flapping])
VALUES (25,'Query slowdown due to availability group','','',1,NULL,'>0',0,0);
GO

INSERT INTO [dbo].[sqlwatch_config_check] ([check_id], [check_name], [check_description], [check_query], [check_frequency_minutes], [check_threshold_warning], [check_threshold_critical], [check_enabled], [ignore_flapping])
VALUES (26,'Availability group replica not healthy','','',1,NULL,'>0',0,0);
GO

INSERT INTO [dbo].[sqlwatch_config_check] ([check_id], [check_name], [check_description], [check_query], [check_frequency_minutes], [check_threshold_warning], [check_threshold_critical], [check_enabled], [ignore_flapping])
VALUES (27,'Availability group replication falling behind','','IF
(
    SELECT redo_queue_size / 1024.0 + log_send_queue_size / 1024.0
    FROM sys.dm_hadr_database_replica_states
) > 100
BEGIN
    SELECT 1;
END;
ELSE
BEGIN
    SELECT 0;
END;',1,NULL,'>0',0,0);
GO

INSERT INTO [dbo].[sqlwatch_config_check] ([check_id], [check_name], [check_description], [check_query], [check_frequency_minutes], [check_threshold_warning], [check_threshold_critical], [check_enabled], [ignore_flapping])
VALUES (28,'Auto Close Enabled','','SELECT COUNT(1)
FROM sys.databases
WHERE is_auto_close_on = 1;',1440,NULL,'>0',0,0);
GO

INSERT INTO [dbo].[sqlwatch_config_check] ([check_id], [check_name], [check_description], [check_query], [check_frequency_minutes], [check_threshold_warning], [check_threshold_critical], [check_enabled], [ignore_flapping])
VALUES (29,'Auto Shrink Enabled','','SELECT COUNT(1)
FROM sys.databases
WHERE is_auto_shrink_on = 1;',1440,NULL,'>0',0,0);
GO

INSERT INTO [dbo].[sqlwatch_config_check] ([check_id], [check_name], [check_description], [check_query], [check_frequency_minutes], [check_threshold_warning], [check_threshold_critical], [check_enabled], [ignore_flapping])
VALUES (30,'Database Mail not running','','IF EXISTS(SELECT 1 FROM sys.configurations WHERE name = Database Mail XPs AND value_in_use = 1)
BEGIN
    EXEC msdb.dbo.sysmail_help_status_sp;
END
ELSE
BEGIN
    SELECT 0
END',1440,NULL,'>0',0,0);
GO

INSERT INTO [dbo].[sqlwatch_config_check] ([check_id], [check_name], [check_description], [check_query], [check_frequency_minutes], [check_threshold_warning], [check_threshold_critical], [check_enabled], [ignore_flapping])
VALUES (31,'Excessive VLFs','','DECLARE @dbccloginfo TABLE
(
    RecoveryUnitId INT,
    fileid SMALLINT,
    file_size BIGINT,
    start_offset BIGINT,
    fseqno INT,
    [status] TINYINT,
    parity TINYINT,
    create_lsn NUMERIC(25, 0)
);
DECLARE @SQLStr NVARCHAR(200) =
        (
            SELECT ''DBCC LogInfo''
        );
INSERT INTO @dbccloginfo
EXEC sp_executesql @SQLStr;',1440,'>800','>1000',0,0);
GO

INSERT INTO [dbo].[sqlwatch_config_check] ([check_id], [check_name], [check_description], [check_query], [check_frequency_minutes], [check_threshold_warning], [check_threshold_critical], [check_enabled], [ignore_flapping])
VALUES (32,'High Memory Usage','','SELECT CONVERT(INT, ((committed_kb/1024.0) / (physical_memory_kb/1024.0)) * 100)
FROM sys.dm_os_sys_info;',5,NULL,'>95',0,0);
GO

INSERT INTO [dbo].[sqlwatch_config_check] ([check_id], [check_name], [check_description], [check_query], [check_frequency_minutes], [check_threshold_warning], [check_threshold_critical], [check_enabled], [ignore_flapping])
VALUES (33,'Identity Column With <5 % Free Keys','','WITH RemainingKeys
AS (SELECT CASE max_length
               WHEN 4 THEN
                   100 - ((CONVERT(INT, last_value) / 2147483647.0) * 100.0)
               WHEN 8 THEN
                   100 - ((CONVERT(BIGINT, last_value) / 9223372036854775807.0) * 100.0)
           END AS PercentFree
    FROM sys.identity_columns
    WHERE last_value IS NOT NULL)
SELECT COUNT(1)
FROM RemainingKeys
WHERE PercentFree < 5.0;',60,NULL,'>0',0,0);
GO

INSERT INTO [dbo].[sqlwatch_config_check] ([check_id], [check_name], [check_description], [check_query], [check_frequency_minutes], [check_threshold_warning], [check_threshold_critical], [check_enabled], [ignore_flapping])
VALUES (34,'Query Store In Unexpected State','','',10,NULL,'>0',0,0);
GO

INSERT INTO [dbo].[sqlwatch_config_check] ([check_id], [check_name], [check_description], [check_query], [check_frequency_minutes], [check_threshold_warning], [check_threshold_critical], [check_enabled], [ignore_flapping])
VALUES (35,'Running Values Dont Match Config Values','','SELECT COUNT(1)
FROM sys.configurations
WHERE [value] <> value_in_use
    AND configuration_id NOT IN (
        SELECT c.configuration_id
        FROM sys.configurations c
        WHERE [name] = ''min server memory (MB)''
            AND ((value_in_use = 8
                AND SERVERPROPERTY(''EngineEdition'') = 4)
                OR (value_in_use = 16
                AND SERVERPROPERTY(''EngineEdition'') != 4))
        );',60,NULL,'>0',0,0);
GO

INSERT INTO [dbo].[sqlwatch_config_check] ([check_id], [check_name], [check_description], [check_query], [check_frequency_minutes], [check_threshold_warning], [check_threshold_critical], [check_enabled], [ignore_flapping])
VALUES (36,'Suspect Pages Found','','SELECT COUNT(1)
FROM msdb.dbo.suspect_pages;',60,NULL,'>0',0,0);
GO

SET IDENTITY_INSERT [dbo].[sqlwatch_config_check] OFF
*/