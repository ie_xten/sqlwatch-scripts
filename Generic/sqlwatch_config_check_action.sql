/*

Enable and edit default checks

*/
/*
--Disable all actions until the check can be configured
UPDATE [dbo].[sqlwatch_config_action]
SET action_enabled = 0

UPDATE che_act
SET che_act.[action_id] = 1 --Send Email to support@xDBA.uk using sp_send_mail  (HTML)
FROM [dbo].[sqlwatch_config_check_action] che_act
INNER JOIN [dbo].[sqlwatch_config_check] che ON che_act.check_id = che.check_id
WHERE che.check_name IN ('Long Running Open Transactions',
'Databases with no LOG backup',
'Databases with no DATA backup',
'Oldest DATA backup (days)',
'Oldest LOG backup (minutes)',
'Database page_verify not CHECKSUM',
'Databases not MULTI_USER',
'Databases not ONLINE',
'Databases with Auto Shrink Enabled',
'Databases with Auto Close Enabled',
'Queued actions have not been processed',
'Checks are failling',
'Check execution time is high',
'One or more disk will be full soon',
'Disk Free % is low',
'Action queue failure rate is high',
'Action queue is high',
'SQL Server Uptime is low',
'CPU Utilistaion',
'Blocking detected',
'Agent Job failure')
AND che.[action_id] = -1

UPDATE che_act
SET che_act.[action_id] = 2 --Send Email to logging@xten.uk using sp_send_mail  (HTML)
FROM [dbo].[sqlwatch_config_check_action] che_act
INNER JOIN [dbo].[sqlwatch_config_check] che ON che_act.check_id = che.check_id
WHERE che.check_name IN ('Log Growths',
'Errors/sec',
'Logins/sec',
'Page life expectancy',
'Buffer cache hit ratio',
'Memory Grants Pending',
'Memory Grants Outstanding',
'Number of Deadlocks/sec',
'Readahead pages/sec',
'Lock Waits/sec',
'Lock Timeouts/sec',
'Lock Requests/sec',
'Average Wait Time (ms)',
'Page writes/sec',
'Page Lookups Rate',
'Page reads/sec',
'Lazy writes/sec',
'Free list stalls/sec',
'Page Split Rate',
'SQL Re-Compilations Rate',
'SQL Compilations Rate',
'Full Scan Rate')
AND che.[action_id] = -1

*/
