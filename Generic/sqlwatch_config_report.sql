/*
https://docs.sqlwatch.io/v/2.2/alerts-and-reports/how-to/get-notified-when-disk-has-low-free-space#add-a-new-check

*/

/*

SET XACT_ABORT ON

USE [SQLWatch]
GO

DELETE FROM [dbo].[sqlwatch_config_report]
WHERE [report_id] > 2 AND [report_id] < 38
GO

SET IDENTITY_INSERT [dbo].[sqlwatch_config_report] ON

--Insert custom checks

INSERT INTO [dbo].[sqlwatch_config_report] ([report_id],[report_title],[report_description],[report_definition],[report_definition_type],[report_active],[report_style_id])
VALUES (3,'Clock Skew Report','','','Query',1,-1)
GO

INSERT INTO [dbo].[sqlwatch_config_report] ([report_id],[report_title],[report_description],[report_definition],[report_definition_type],[report_active],[report_style_id])
VALUES (4,'Remote Data Collection is Failing Report','',  'SELECT sql_ins.[sql_instance]
      ,sql_ins.[hostname]
      ,sql_ins.[sql_port]
      ,sql_ins.[sqlwatch_database_name]
      ,sql_ins.[environment]
      ,sql_ins.[is_active]
      ,sql_ins.[last_collection_time]
      ,sql_ins.[collection_status]
FROM [dbo].[sqlwatch_logger_check] log_che
INNER JOIN [sqlwatch_config_sql_instance] sql_ins ON sql_ins.[sql_instance] = log_che.[sql_instance]
WHERE sql_ins.[collection_status] != ''Success''
AND sql_ins.[collection_status] IS NOT NULL
AND sql_ins.[is_active] = 1','Query',1,-1)
GO

INSERT INTO [dbo].[sqlwatch_config_report] ([report_id],[report_title],[report_description],[report_definition],[report_definition_type],[report_active],[report_style_id])
VALUES (5,'Agent Job duration longer than expected Report','','','Query',1,-1)
GO

INSERT INTO [dbo].[sqlwatch_config_report] ([report_id],[report_title],[report_description],[report_definition],[report_definition_type],[report_active],[report_style_id])
VALUES (6,'Data I/O utilization % in last 10 minutes Report','','','Query',1,-1)
GO

INSERT INTO [dbo].[sqlwatch_config_report] ([report_id],[report_title],[report_description],[report_definition],[report_definition_type],[report_active],[report_style_id])
VALUES (7,'Database Free Space <5% (MB) Report','','SELECT (((size / 128.0) - (CAST(FILEPROPERTY(name,''SpaceUsed'') AS INT) / 128.0)) / (size / 128.0)) * 100 AS''PercRemaining'', type_desc, name, physical_name, state_desc, CONVERT(DECIMAL(18, 4), SIZE / (128)) AS''Size(MB)'', max_size, CASE 
		WHEN is_percent_growth = 0
			THEN CONVERT(DECIMAL(18, 4), growth / (128))
		ELSE growth
		END AS''Growth(MB / %)'', is_percent_growth, is_read_only
FROM sys.database_files
WHERE type_desc =''ROWS''
	OR type_desc =''LOG''
	AND (((size / 128.0) - (CAST(FILEPROPERTY(name,''SpaceUsed'') AS INT) / 128.0)) / (size / 128.0)) * 100 < 5.0;','Query',1,-1)
GO

IF EXISTS (SELECT 1	FROM sys.system_objects	WHERE [name] LIKE 'sys.dm_db_resource_stats') --Azure Only
BEGIN
      INSERT INTO [dbo].[sqlwatch_config_report] ([report_id],[report_title],[report_description],[report_definition],[report_definition_type],[report_active],[report_style_id])
      VALUES (8,'DTU utilization % in last 10 minutes Report','','','Query',1,-1)
END
GO

IF EXISTS (SELECT 1	FROM sys.system_objects	WHERE [name] LIKE 'sys.dm_db_resource_stats') --Azure Only
BEGIN
      INSERT INTO [dbo].[sqlwatch_config_report] ([report_id],[report_title],[report_description],[report_definition],[report_definition_type],[report_active],[report_style_id])
      VALUES (9,'Log I/O utilization % in the last 10 minutes Report','','','Query',1,-1)
END
GO

IF EXISTS (SELECT 1	FROM sys.system_objects	WHERE [name] LIKE 'sys.dm_db_resource_stats') --Azure Only
BEGIN
      INSERT INTO [dbo].[sqlwatch_config_report] ([report_id],[report_title],[report_description],[report_definition],[report_definition_type],[report_active],[report_style_id])
      VALUES (10,'Session limit % in the last 10 minutes Report','','','Query',1,-1)
END
GO

IF EXISTS (SELECT 1	FROM sys.system_objects	WHERE [name] LIKE 'sys.dm_db_resource_stats') --Azure Only
BEGIN
      INSERT INTO [dbo].[sqlwatch_config_report] ([report_id],[report_title],[report_description],[report_definition],[report_definition_type],[report_active],[report_style_id])
      VALUES (11,'Worker thread limit % in the last 10 minutes Report','','','Query',1,-1)
END
GO

INSERT INTO [dbo].[sqlwatch_config_report] ([report_id],[report_title],[report_description],[report_definition],[report_definition_type],[report_active],[report_style_id])
VALUES (12,'IO Stalls detected Report','','SELECT Drive, volume_mount_point,[file_id],[type_desc], [physical_name], state_desc,num_of_reads,io_stall_read_ms,num_of_writes,io_stall_write_ms,num_of_bytes_read, num_of_bytes_written, io_stall, [io_stall] / ([num_of_reads] + [num_of_writes]) AS ''% I/O Stalls''
FROM (
	SELECT LEFT(UPPER(mf.physical_name), 2) AS Drive, mf.[type_desc], mf.[physical_name], mf.state_desc, SUM(num_of_reads) AS num_of_reads, SUM(io_stall_read_ms) AS io_stall_read_ms, SUM(num_of_writes) AS num_of_writes, SUM(io_stall_write_ms) AS io_stall_write_ms, SUM(num_of_bytes_read) AS num_of_bytes_read, SUM(num_of_bytes_written) AS num_of_bytes_written, SUM(io_stall) AS io_stall, vs.volume_mount_point, vfs.file_id
	FROM sys.dm_io_virtual_file_stats(NULL, NULL) AS vfs
	INNER JOIN sys.master_files AS mf WITH (NOLOCK) ON vfs.database_id = mf.database_id
		AND vfs.[file_id] = mf.[file_id]
	CROSS APPLY sys.dm_os_volume_stats(mf.database_id, mf.[file_id]) AS vs
	GROUP BY LEFT(UPPER(mf.physical_name), 2), vs.volume_mount_point, vfs.[file_id], mf.[type_desc], mf.[physical_name], mf.state_desc
	) AS tab
WHERE tab.[file_id] <> 2
AND [io_stall] / ([num_of_reads] + [num_of_writes]) > 0','Query',1,-1)
GO

INSERT INTO [dbo].[sqlwatch_config_report] ([report_id],[report_title],[report_description],[report_definition],[report_definition_type],[report_active],[report_style_id])
VALUES (13,'Days until Database file is full Report','','','Query',1,-1)
GO

INSERT INTO [dbo].[sqlwatch_config_report] ([report_id],[report_title],[report_description],[report_definition],[report_definition_type],[report_active],[report_style_id])
VALUES (14,'Replication Commands Report','','SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED;

USE [distribution]

SELECT TOP 1 COUNT(1) AS Commands
FROM MSrepl_commands cmds
    INNER JOIN MSarticles a
        ON a.article_id = cmds.article_id
    INNER JOIN MSrepl_transactions t
        ON cmds.xact_seqno = t.xact_seqno
GROUP BY CONVERT(SMALLDATETIME, t.entry_time)
ORDER BY CONVERT(SMALLDATETIME, t.entry_time) DESC;','Query',1,-1)
GO

INSERT INTO [dbo].[sqlwatch_config_report] ([report_id],[report_title],[report_description],[report_definition],[report_definition_type],[report_active],[report_style_id])
VALUES (15,'Failed Logins in the past 60 Mins Report','','','Query',1,-1)
GO

INSERT INTO [dbo].[sqlwatch_config_report] ([report_id],[report_title],[report_description],[report_definition],[report_definition_type],[report_active],[report_style_id])
VALUES (16,'SQL Server Full Text Search Service Stopped Report','','','Query',1,-1)
GO

INSERT INTO [dbo].[sqlwatch_config_report] ([report_id],[report_title],[report_description],[report_definition],[report_definition_type],[report_active],[report_style_id])
VALUES (17,'SQL Server Integration Service Stopped Report','','','Query',1,-1)
GO

INSERT INTO [dbo].[sqlwatch_config_report] ([report_id],[report_title],[report_description],[report_definition],[report_definition_type],[report_active],[report_style_id])
VALUES (18,'SQL Server VSS Writer Service Stopped Report','','','Query',1,-1)
GO

INSERT INTO [dbo].[sqlwatch_config_report] ([report_id],[report_title],[report_description],[report_definition],[report_definition_type],[report_active],[report_style_id])
VALUES (19,'SQL Server Analysis Service Stopped Report','','','Query',1,-1)
GO

INSERT INTO [dbo].[sqlwatch_config_report] ([report_id],[report_title],[report_description],[report_definition],[report_definition_type],[report_active],[report_style_id])
VALUES (20,'SQL Server Reporting Service Stopped Report','','','Query',1,-1)
GO

INSERT INTO [dbo].[sqlwatch_config_report] ([report_id],[report_title],[report_description],[report_definition],[report_definition_type],[report_active],[report_style_id])
VALUES (21,'Availability group database not healthy Report','','','Query',1,-1)
GO

INSERT INTO [dbo].[sqlwatch_config_report] ([report_id],[report_title],[report_description],[report_definition],[report_definition_type],[report_active],[report_style_id])
VALUES (22,'Availability group failover Report','','','Query',1,-1)
GO

INSERT INTO [dbo].[sqlwatch_config_report] ([report_id],[report_title],[report_description],[report_definition],[report_definition_type],[report_active],[report_style_id])
VALUES (23,'Availability group listener offline Report','','','Query',1,-1)
GO

INSERT INTO [dbo].[sqlwatch_config_report] ([report_id],[report_title],[report_description],[report_definition],[report_definition_type],[report_active],[report_style_id])
VALUES (24,'Availability group not ready for failover Report','','','Query',1,-1)
GO

INSERT INTO [dbo].[sqlwatch_config_report] ([report_id],[report_title],[report_description],[report_definition],[report_definition_type],[report_active],[report_style_id])
VALUES (25,'Query slowdown due to availability group Report','','','Query',1,-1)
GO

INSERT INTO [dbo].[sqlwatch_config_report] ([report_id],[report_title],[report_description],[report_definition],[report_definition_type],[report_active],[report_style_id])
VALUES (26,'Availability group replica not healthy Report','','','Query',1,-1)
GO

INSERT INTO [dbo].[sqlwatch_config_report] ([report_id],[report_title],[report_description],[report_definition],[report_definition_type],[report_active],[report_style_id])
VALUES (27,'Availability group replication falling behind Report','','','Query',1,-1)
GO

INSERT INTO [dbo].[sqlwatch_config_report] ([report_id],[report_title],[report_description],[report_definition],[report_definition_type],[report_active],[report_style_id])
VALUES (28,'Auto Close Enabled Report','','','Query',1,-1)
GO

INSERT INTO [dbo].[sqlwatch_config_report] ([report_id],[report_title],[report_description],[report_definition],[report_definition_type],[report_active],[report_style_id])
VALUES (29,'Auto Shrink Enabled Report','','','Query',1,-1)
GO

INSERT INTO [dbo].[sqlwatch_config_report] ([report_id],[report_title],[report_description],[report_definition],[report_definition_type],[report_active],[report_style_id])
VALUES (30,'Database Mail not running Report','','','Query',1,-1)
GO

INSERT INTO [dbo].[sqlwatch_config_report] ([report_id],[report_title],[report_description],[report_definition],[report_definition_type],[report_active],[report_style_id])
VALUES (31,'Excessive VLFs Report','','','Query',1,-1)
GO

INSERT INTO [dbo].[sqlwatch_config_report] ([report_id],[report_title],[report_description],[report_definition],[report_definition_type],[report_active],[report_style_id])
VALUES (32,'High Memory Usage Report','','','Query',1,-1)
GO

INSERT INTO [dbo].[sqlwatch_config_report] ([report_id],[report_title],[report_description],[report_definition],[report_definition_type],[report_active],[report_style_id])
VALUES (33,'Identity Column With <5 % Free Keys Report','','','Query',1,-1)
GO

INSERT INTO [dbo].[sqlwatch_config_report] ([report_id],[report_title],[report_description],[report_definition],[report_definition_type],[report_active],[report_style_id])
VALUES (34,'Query Store In Unexpected State Report','','','Query',1,-1)
GO

INSERT INTO [dbo].[sqlwatch_config_report] ([report_id],[report_title],[report_description],[report_definition],[report_definition_type],[report_active],[report_style_id])
VALUES (35,'Running Values Dont Match Config Values Report','','','Query',1,-1)
GO

INSERT INTO [dbo].[sqlwatch_config_report] ([report_id],[report_title],[report_description],[report_definition],[report_definition_type],[report_active],[report_style_id])
VALUES (36,'Suspect Pages Found Report','','','Query',1,-1)

SET IDENTITY_INSERT [dbo].[sqlwatch_config_report] OFF

*/